# Contributing to MoFEM 

#### Table of links to follow

* MoFEM is a software with an open community of users and developers, and here we put what it means to be a good citizen of MoFEM community. [Link to Be a good MoFEM citizen](http://mofem.eng.gla.ac.uk/mofem/html/being_a_good_citizen.html)

* MoFEM has its own naming conventions and guidance which you should follow when
you program, make pull requests,  etc. Please follow [link to coding practice](http://mofem.eng.gla.ac.uk/mofem/html/coding_practice.html) for more details.

* MoFEM has bugs and there are rules on how to report them. You can ask for enhancements, proposals, and tasks [link](https://bitbucket.org/likask/mofem-cephas/issues?status=new&status=open); please follow 
[link to bug reporting](http://mofem.eng.gla.ac.uk/mofem/html/guidelines_bug_reporting.html) for more details.

* [Writing documentation](http://mofem.eng.gla.ac.uk/mofem/html/a_guide_to_writing_documentation.html) is essential to develop the code successfully; without comments, examples and tutorials code will be difficult or even impossible to use.

* If you seek support, email us on [Q&A group](https://groups.google.com/forum/#!categories/mofem-group) or join [MoFEM Slack](https://mofem.slack.com).
 


We encourage you to join our team!